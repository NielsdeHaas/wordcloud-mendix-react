define([
    "MyWidget/widget/lib/react", "MyWidget/widget/lib/react-dom"
], function(React, ReactDOM) {
    return React.createClass({
        // the state is what triggers the real-time DOM updates
        // ---
        // Here we're just initializing it with some default values
        // to-do: remove the state here.
        getInitialState: function() {
            return {list: []}
        },

        // Here, we call a microflow to fetch our data, and store that in the
        // state
        // 
        // Test
        handleCostructorClick: function(e){
            var self = this;
            debugger;
            mx.data.action({
                params: {
                    actionname: "TestSuite.Constructor_detail",
                    applyto: "selection",
                    guids: [this.state.list[0].get("TestSuite.Year_Constructor")]
                },
                callback: function (res) {
                    console.log(res)
                },
                error: function (err) {
                    console.log(err)
                }
            }, this)

        },

        render: function() {
            debugger;
            var listOfConstructors = this.props.list.map(function(s) {
                var liStyle = { "color": "white", "padding": "8px", "margin" : "4px", "background-color": "black"}
                return React.createElement("span", {style: liStyle}, 
                    s.get('Name').toString(), " -- ", s.get('Points').toString()
                )
            });
            return (
                React.createElement("div", null, 
                    listOfConstructors
                )

            )
        }

    });

});
