define([
  "MyWidget/widget/lib/react",
  "MyWidget/widget/lib/react-dom",
  "MyWidget/components/js/Child"
], function (React, ReactDOM, Child) {
  return React.createClass({

    getInitialState: function() {
      return { MxContext: {} , list: []}
  },

    componentWillMount: function () {
      var self = this;
      mx.data.action({
          params: {
              actionname: this.props.dojoContext.getDataMF, 
              applyto: "selection",
              guids: [this.props.MxContexObj.getGuid()]
          },
          callback: function(res) {
              console.log(res);
              self.setState({MxContext: self.props.MxContexObj , list: res});
          },
          error: function(err) {
              console.log(err)
          }
      }, this)
  },

    // listeners
    // ---
    // fired when a user wants to load the previous years data.
    handlePreviousYear: function (e) {
      var self = this;
      mx.data.action({
        params: {
          actionname: "TestSuite.DS_loadPreviousYear",
          applyto: "selection",
          guids: [this.state.MxContext._guid]
        },
        callback: function (res) {
          console.log(res)
          if (res && res.length > 0)
            self.setState({ MxContext: res[0] });
            self.getConstructorsforYear(res);
        },
        error: function (err) {
          console.log(err)
        }
      }, this)
    },
    // -----
    //  Fired when a user clicks the next year button.
    handleNextYear: function (e) {
      var self = this
      mx.data.action({
        params: {
          actionname: "TestSuite.DS_loadNextYear",
          applyto: "selection",
          guids: [this.state.MxContext._guid]
        },
        callback: function (res) {
          console.log(res)
          if (res && res.length > 0)
            self.setState({ MxContext: res[0] });
            self.getConstructorsforYear();
        },
        error: function (err) {
          console.log(err)
        }
      }, this)
    },

    getConstructorsforYear: function (e) {
      var self = this
      mx.data.action({
        params: {
          actionname: this.props.dojoContext.getDataMF,
          applyto: "selection",
          guids: [this.state.MxContext._guid]
        },
        callback: function (res) {
          console.log(res)
          if (res && res.length > 0)
            self.setState({ list: res })
        },
        error: function (err) {
          console.log(err)
        }
      }, this)
    },

    render: function () {
      debugger;
      return (
        React.createElement("div", null, 
          React.createElement("h3", null, "Pick your favorite team from ", this.props.MxContexObj.get("Year").toString()), 
          React.createElement(Child, {list: this.state.list, dojoContext: this.props.dojoContext}), 
          React.createElement("div", null, 
            React.createElement("br", null), 
            React.createElement("input", {type: "button", value: "Next year", onClick: this.handleNextYear}), 
            React.createElement("br", null), 
            React.createElement("input", {type: "button", value: "Previous year", onClick: this.handlePreviousYear})
          )
        )
      )
    }
  })
});
