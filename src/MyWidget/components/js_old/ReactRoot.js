define([
  "MyWidget/widget/lib/react",
  "MyWidget/widget/lib/react-dom",
  "MyWidget/components/js/Child"
], function(React, ReactDOM, Child) {
    return React.createClass({
      render: function(){
        return (
          React.createElement("div", null, 
            React.createElement("h3", null, "Ranking"), 
            React.createElement(Child, {year: this.props.year})
          )
        )
      }
    })
});
