define([
    "MyWidget/widget/lib/react", "MyWidget/widget/lib/react-dom"
], function (React, ReactDOM) {
    return React.createClass({
        // the state is what triggers the real-time DOM updates
        // ---
        // Here we're just initializing it with some default values
        getInitialState: function () {
            return { list: [], newPetName: '', newPetType: 'cat' }
        },

        // componentWillMount is called immediately before the component is
        // rendered to the DOM.
        // ---
        // Here, we call a microflow to fetch our data, and store that in the
        // state
        componentWillMount: function () {
            var self = this
            mx.data.action({
                params: {
                    actionname: "TestSuite.DS_ReturnList",
                    applyto: "selection",
                    guids: [this.props.year.getGuid()]
                },
                callback: function (res) {
                    console.log(res)
                    self.setState({ list: res })
                },
                error: function (err) {
                    console.log(err)
                }
            }, this)
        },
        // listeners
        // ---
        // Fired when a user clicks the previous year button.
        handlePreviousYear: function (e) {
            var self = this;
            mx.data.action({
                params: {
                    actionname: "TestSuite.DS_loadPreviousYear",
                    applyto: "selection",
                    guids: [this.state.list[0].get("TestSuite.Year_Constructor")]
                },
                callback: function (res) {
                    console.log(res)
                    if (res.length > 0 ) 
                        self.setState({ list: res })
                },
                error: function (err) {
                    console.log(err)
                }
            }, this)
        },
        // -----
        //  Fired when a user clicks the next year button.
        handleNextYear: function () {
            var self = this
            mx.data.action({
                params: {
                    actionname: "TestSuite.DS_loadNextYear",
                    applyto: "selection",
                    guids: [this.state.list[0].get("TestSuite.Year_Constructor")]
                },
                callback: function (res) {
                    console.log(res)
                    if (res.length > 0 ) 
                        self.setState({ list: res })
                },
                error: function (err) {
                    console.log(err)
                }
            }, this)
        },

        handleCostructorClick: function(e){
            var self = this;
            debugger;
            mx.data.action({
                params: {
                    actionname: "TestSuite.Constructor_detail",
                    applyto: "selection",
                    guids: [this.state.list[0].get("TestSuite.Year_Constructor")]
                },
                callback: function (res) {
                    console.log(res)
                    self.setState({ list: res })
                },
                error: function (err) {
                    console.log(err)
                }
            }, this)

        },

        render: function () {
            // debugger;
            var listOfConstructors = this.state.list.map(function (s) {
                // style: {fontsize: (20 + s.get('Points') * 2.5) }
                return React.createElement("li", { key: s._guid , onClick: this.handleCostructorClick },
                    s.get('Name'), " -- ", s.get('Points').toString()
                )
            });
            return (
                React.createElement("div", null,
                    React.createElement("ul", null, listOfConstructors),
                    React.createElement("div", null,
                        React.createElement("br", null),
                        React.createElement("input", { type: "button", value: "Next year", onClick: this.handleNextYear }),
                        React.createElement("br", null),
                        React.createElement("input", { type: "button", value: "Previous year", onClick: this.handlePreviousYear })
                    )
                )

            )
        }

    });

});
